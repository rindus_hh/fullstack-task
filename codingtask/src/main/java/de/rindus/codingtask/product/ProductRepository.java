package de.rindus.codingtask.product;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by: phil on 11.08.17.
 */
public interface ProductRepository extends MongoRepository<Product, String> {
}
