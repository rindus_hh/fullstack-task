package de.rindus.codingtask.product;

import org.springframework.data.annotation.Id;

/**
 * Created by: phil on 11.08.17.
 */
public class Product {

    @Id
    private String id;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
